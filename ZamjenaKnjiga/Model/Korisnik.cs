﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ZamjenaKnjiga.Model
{
    public partial class Korisnik
    {
        public Korisnik()
        {
            Oglas = new HashSet<Oglas>();
        }

        public string Username { get; set; }
        public string Password { get; set; }
        public int IdUser { get; set; }
        public string Email { get; set; }
        public string Telefon { get; set; }
        public string Uloga { get; set; }
        public string Adresa { get; set; }
        public int PostanskiBroj { get; set; }

        public virtual Grad PostanskiBrojNavigation { get; set; }
        public virtual ICollection<Oglas> Oglas { get; set; }
    }
}
