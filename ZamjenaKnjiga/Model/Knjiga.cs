﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ZamjenaKnjiga.Model
{
    public partial class Knjiga
    {
        public Knjiga()
        {
            JeZanra = new HashSet<JeZanra>();
            Oglas = new HashSet<Oglas>();
        }

        public string Naziv { get; set; }
        public int IdKnjiga { get; set; }
        public int GodinaIzdanja { get; set; }
        public string Jezik { get; set; }
        public string Pisac { get; set; }
        public string Ocuvanost { get; set; }

        public virtual ICollection<JeZanra> JeZanra { get; set; }
        public virtual ICollection<Oglas> Oglas { get; set; }
    }
}
