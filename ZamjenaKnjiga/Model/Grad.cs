﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ZamjenaKnjiga.Model
{
    public partial class Grad
    {
        public Grad()
        {
            Korisnik = new HashSet<Korisnik>();
        }

        public int PostanskiBroj { get; set; }
        public string ImeGrada { get; set; }
        public string FkDrzava { get; set; }

        public virtual Drzava FkDrzavaNavigation { get; set; }
        public virtual ICollection<Korisnik> Korisnik { get; set; }
    }
}
