﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ZamjenaKnjiga.Model
{
    public partial class Drzava
    {
        public Drzava()
        {
            Grad = new HashSet<Grad>();
        }

        public string IsoDrzava { get; set; }
        public string ImeDrzave { get; set; }

        public virtual ICollection<Grad> Grad { get; set; }
    }
}
