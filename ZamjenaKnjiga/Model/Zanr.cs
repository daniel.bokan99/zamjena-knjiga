﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ZamjenaKnjiga.Model
{
    public partial class Zanr
    {
        public Zanr()
        {
            JeZanra = new HashSet<JeZanra>();
        }

        public int IdZanr { get; set; }
        public string Naziv { get; set; }

        public virtual ICollection<JeZanra> JeZanra { get; set; }
    }
}
