﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ZamjenaKnjiga.Model
{
    public partial class Oglas
    {
        public int IdOglas { get; set; }
        public string Naslov { get; set; }
        public DateTime DatumObjave { get; set; }
        public bool Aktivan { get; set; }
        public bool JeZamjena { get; set; }
        public int? Cijena { get; set; }
        public string Opis { get; set; }
        public int FkUser { get; set; }
        public int FkKnjiga { get; set; }

        public virtual Knjiga FkKnjigaNavigation { get; set; }
        public virtual Korisnik FkUserNavigation { get; set; }
    }
}
