﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace ZamjenaKnjiga.Model
{
    public partial class ProjektDatabaseContext : DbContext
    {
        public ProjektDatabaseContext(DbContextOptions<ProjektDatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Drzava> Drzava { get; set; }
        public virtual DbSet<Grad> Grad { get; set; }
        public virtual DbSet<JeZanra> JeZanra { get; set; }
        public virtual DbSet<Knjiga> Knjiga { get; set; }
        public virtual DbSet<Korisnik> Korisnik { get; set; }
        public virtual DbSet<Oglas> Oglas { get; set; }
        public virtual DbSet<Zanr> Zanr { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Drzava>(entity =>
            {
                entity.HasKey(e => e.IsoDrzava)
                    .HasName("PK__Drzava__26BBC7F1A1DDCC96");

                entity.ToTable("Drzava");

                entity.Property(e => e.IsoDrzava)
                    .HasMaxLength(3)
                    .HasColumnName("ISO_drzava");

                entity.Property(e => e.ImeDrzave)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Grad>(entity =>
            {
                entity.HasKey(e => e.PostanskiBroj)
                    .HasName("PK__Grad__9FDD638D6CB54054");

                entity.ToTable("Grad");

                entity.Property(e => e.PostanskiBroj).ValueGeneratedNever();

                entity.Property(e => e.FkDrzava)
                    .IsRequired()
                    .HasMaxLength(3)
                    .HasColumnName("FK_Drzava");

                entity.Property(e => e.ImeGrada)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.FkDrzavaNavigation)
                    .WithMany(p => p.Grad)
                    .HasForeignKey(d => d.FkDrzava)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Grad__FK_Drzava__656C112C");
            });

            modelBuilder.Entity<JeZanra>(entity =>
            {
                entity.HasKey(e => new { e.IdKnjiga, e.IdZanr })
                    .HasName("PK__JeZanra__E35DACBA148C756B");

                entity.ToTable("JeZanra");

                entity.Property(e => e.IdKnjiga).HasColumnName("Id_Knjiga");

                entity.Property(e => e.IdZanr).HasColumnName("Id_Zanr");

                entity.HasOne(d => d.IdKnjigaNavigation)
                    .WithMany(p => p.JeZanra)
                    .HasForeignKey(d => d.IdKnjiga)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__JeZanra__Id_Knji__6C190EBB");

                entity.HasOne(d => d.IdZanrNavigation)
                    .WithMany(p => p.JeZanra)
                    .HasForeignKey(d => d.IdZanr)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__JeZanra__Id_Zanr__6D0D32F4");
            });

            modelBuilder.Entity<Knjiga>(entity =>
            {
                entity.HasKey(e => e.IdKnjiga)
                    .HasName("PK__Knjiga__E555B7D3B89C60A0");

                entity.ToTable("Knjiga");

                entity.Property(e => e.IdKnjiga).HasColumnName("Id_Knjiga");

                entity.Property(e => e.Jezik)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Ocuvanost)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Pisac)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Korisnik>(entity =>
            {
                entity.HasKey(e => e.IdUser)
                    .HasName("PK__Korisnik__D03DEDCBF344AFA8");

                entity.ToTable("Korisnik");

                entity.HasIndex(e => e.Username, "UQ__Korisnik__536C85E42D35B861")
                    .IsUnique();

                entity.HasIndex(e => e.Email, "UQ__Korisnik__A9D10534B04E80EF")
                    .IsUnique();

                entity.Property(e => e.IdUser).HasColumnName("Id_User");

                entity.Property(e => e.Adresa)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Telefon)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Uloga)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.PostanskiBrojNavigation)
                    .WithMany(p => p.Korisnik)
                    .HasForeignKey(d => d.PostanskiBroj)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Korisnik__Postan__71D1E811");
            });

            modelBuilder.Entity<Oglas>(entity =>
            {
                entity.HasKey(e => e.IdOglas)
                    .HasName("PK__Oglas__BF692490AB33819E");

                entity.Property(e => e.IdOglas).HasColumnName("Id_Oglas");

                entity.Property(e => e.DatumObjave).HasColumnType("date");

                entity.Property(e => e.FkKnjiga).HasColumnName("FK_Knjiga");

                entity.Property(e => e.FkUser).HasColumnName("FK_User");

                entity.Property(e => e.Naslov)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Opis).HasMaxLength(300);

                entity.HasOne(d => d.FkKnjigaNavigation)
                    .WithMany(p => p.Oglas)
                    .HasForeignKey(d => d.FkKnjiga)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Oglas__FK_Knjiga__75A278F5");

                entity.HasOne(d => d.FkUserNavigation)
                    .WithMany(p => p.Oglas)
                    .HasForeignKey(d => d.FkUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Oglas__FK_User__74AE54BC");
            });

            modelBuilder.Entity<Zanr>(entity =>
            {
                entity.HasKey(e => e.IdZanr)
                    .HasName("PK__Zanr__6081B69142AC1ABA");

                entity.ToTable("Zanr");

                entity.Property(e => e.IdZanr).HasColumnName("Id_Zanr");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
