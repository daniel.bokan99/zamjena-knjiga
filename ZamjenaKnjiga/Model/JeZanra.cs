﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ZamjenaKnjiga.Model
{
    public partial class JeZanra
    {
        public int IdKnjiga { get; set; }
        public int IdZanr { get; set; }

        public virtual Knjiga IdKnjigaNavigation { get; set; }
        public virtual Zanr IdZanrNavigation { get; set; }
    }
}
