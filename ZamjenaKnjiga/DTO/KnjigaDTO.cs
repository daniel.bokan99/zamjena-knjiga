﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZamjenaKnjiga.Model;

namespace ZamjenaKnjiga.DTO {
    public class KnjigaDTO {

        public KnjigaDTO() { }

        public KnjigaDTO(Knjiga knjiga) {
            IdKnjiga = knjiga.IdKnjiga;
            Naziv = knjiga.Naziv;
            GodinaIzdanja = knjiga.GodinaIzdanja;
            Jezik = knjiga.Jezik;
            Pisac = knjiga.Pisac;
            Ocuvanost = knjiga.Ocuvanost;
        }

        public int IdKnjiga { get; set; }

        public string Naziv { get; set; }

        public int GodinaIzdanja { get; set; }

        public string Jezik { get; set; }

        public string Pisac { get; set; }

        public string Ocuvanost { get; set; }
    }
}
