﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZamjenaKnjiga.Model;

namespace ZamjenaKnjiga.DTO {
    public class ZanrDTO {

        public ZanrDTO() { }

        public ZanrDTO(Zanr z) {
            IdZanra = z.IdZanr;
            Naziv = z.Naziv;
        }

        public int IdZanra { get; set; }

        public string Naziv { get; set; }

    }
}
