﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZamjenaKnjiga.Model;

namespace ZamjenaKnjiga.DTO {
    public class OglasDTO {

        public OglasDTO() { }

        public OglasDTO(Oglas o) {
            IdOglas = o.IdOglas;
            Naslov = o.Naslov;
            DatumObjave = o.DatumObjave;
            Aktivan = o.Aktivan;
            JeZamjena = o.JeZamjena;
            Cijena = o.Cijena;
            Opis = o.Opis;
            FkUser = o.FkUser;
            FkKnjiga = o.FkKnjiga;
        }
        public int IdOglas { get; set; }
        public string Naslov { get; set; }
        public DateTime DatumObjave { get; set; }
        public bool Aktivan { get; set; }
        public bool JeZamjena { get; set; }
        public int? Cijena { get; set; }
        public string Opis { get; set; }
        public int FkUser { get; set; }
        public int FkKnjiga { get; set; }

    }
}
