﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZamjenaKnjiga.DTO;
using ZamjenaKnjiga.Model;

namespace ZamjenaKnjiga.Controllers {
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class KnjigaController : ControllerBase {

        private readonly ProjektDatabaseContext ctx;

        public KnjigaController(ProjektDatabaseContext ctx) {
            this.ctx = ctx;
        }

        //[Authorize]
        [HttpPost]
        public async Task<JsonResult> dodajKnjigu(Knjiga k) {

            Knjiga knjiga = await ctx.Knjiga.Where(knj => knj.IdKnjiga == k.IdKnjiga).FirstOrDefaultAsync();

            if(knjiga == null) {

                try {
                    await ctx.AddAsync(k);
                    await ctx.SaveChangesAsync();
                }catch(Exception e) {
                    return new JsonResult(new {
                        Message = "Pogreška s bazom prilikom dodavanje knjige."
                    });
                }

                return new JsonResult(new {
                    Message = "Knjiga uspješno dodana u bazu.",
                    Knjiga = k
                });

            } else {

                knjiga.Naziv = k.Naziv;
                knjiga.GodinaIzdanja = k.GodinaIzdanja;
                knjiga.Jezik = k.Jezik;
                knjiga.Ocuvanost = k.Ocuvanost;
                knjiga.Pisac = k.Pisac;

                try {
                    await ctx.SaveChangesAsync();
                }catch(Exception e) {
                    return new JsonResult(new {
                        Message = "Pogreška prilikom ažuriranja u bazu."
                    });
                }

                return new JsonResult(new {
                    Message = "Knjiga uspješno ažurirana.",
                    Knjiga = new KnjigaDTO(knjiga)
                });
            }

        }

        //[Authorize]
        [HttpPost]
        public async Task<JsonResult> dodajZanrZaKnjigu(JeZanra z) {

            try{
                await ctx.JeZanra.AddAsync(z);
                await ctx.SaveChangesAsync();

                var knjiga = await ctx.Knjiga.FindAsync(z.IdKnjiga);
                var zanr = await ctx.Zanr.FindAsync(z.IdZanr);

                return new JsonResult(new {
                    Message = "Uspješno dodan žanr za knjigu.",
                    Knjiga = new KnjigaDTO(knjiga),
                    Zanr = new ZanrDTO(zanr)
                });

            } catch(Exception e) {

                return new JsonResult(new {
                    Message = "Pogreška prilikom dodavanja žanra za knjigu"
                });
            }
        }

        [HttpGet]
        public async Task<JsonResult> zanrovi() {

            try {
                var zanrovi = await ctx.Zanr.ToListAsync();

                return new JsonResult(new {
                    Message = "Uspješno dohvaćeni zanrovi",
                    Zanrovi = zanrovi
                });

            }catch(Exception e) {
                return new JsonResult(new {
                    Message = "Pogreška prilikom dohvaćanja zanrova iz baze."
                });
            }
        }

        [HttpGet]
        public async Task<JsonResult> knjiga(int id) {

            try {
                var knjiga = await ctx.Knjiga.Where(k => k.IdKnjiga == id)
                                             .Include(k => k.JeZanra)
                                             .ThenInclude(z => z.IdZanrNavigation)
                                             .FirstOrDefaultAsync();

                if(knjiga != null) {
                    return new JsonResult(new {
                        Message = "Uspješno vraćena knjiga pod id-em " + id,
                        Knjiga = knjiga
                    });
                } else {
                    return new JsonResult(new {
                        Message = "Knjiga pod id-em " + id + " ne postoji"
                    });
                }
            } catch (Exception e) {
                return new JsonResult(new {
                    Message = "Pogreška prilikom dohvaćanja knjige pod id-em " + id
                });
            }

        }

    }
}
