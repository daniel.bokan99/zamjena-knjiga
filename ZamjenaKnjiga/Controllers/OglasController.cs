﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZamjenaKnjiga.DTO;
using ZamjenaKnjiga.Model;

namespace ZamjenaKnjiga.Controllers {
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class OglasController : ControllerBase {

        private readonly ProjektDatabaseContext ctx;

        public OglasController(ProjektDatabaseContext ctx) {
            this.ctx = ctx;
        }

        //[Authorize]
        [HttpPost]
        public async Task<JsonResult> dodajOglas(Oglas o) {

            var oglas = await ctx.Oglas.FindAsync(o.IdOglas);

            if(oglas == null) {

                Console.WriteLine(o.FkUser);

                try {
                    o.DatumObjave = DateTime.Now;
                    await ctx.Oglas.AddAsync(o);
                    await ctx.SaveChangesAsync();

                    return new JsonResult(new {
                        Message = "Oglas uspješno dodan",
                        Oglas = new OglasDTO(o)
                    });

                }catch(Exception e) {

                    return new JsonResult(new {
                        Message = "Pogreška prilikom dodavanja oglasa u bazu."
                    });
                }
            } else {

                oglas.IdOglas = o.IdOglas;
                oglas.Naslov = o.Naslov;
                oglas.DatumObjave = DateTime.Now;
                oglas.Aktivan = o.Aktivan;
                oglas.JeZamjena = o.JeZamjena;
                oglas.Cijena = o.Cijena;
                oglas.Opis = o.Opis;
                oglas.FkUser = o.FkUser;
                oglas.FkKnjiga = o.FkKnjiga;

                try {
                    await ctx.SaveChangesAsync();

                    return new JsonResult(new {
                        Message = "Oglas uspješno ažuriran",
                        Oglas = new OglasDTO(o)
                    });
                }catch(Exception e) {
                    return new JsonResult(new {
                        Message = "Pogreška prilikom ažuriranja oglasa"
                    });
                }
            }
        }

        //[Authorize]
        [HttpDelete]
        public async Task<JsonResult> izbrisiOglas(int id) {

            try {
                var oglas = await ctx.Oglas.FindAsync(id);
                if(oglas != null) {
                    ctx.Oglas.Remove(oglas);
                    await ctx.SaveChangesAsync();
                    return new JsonResult(new {
                        Message = "Oglas uspješno izbrisan",
                        Oglas = new OglasDTO(oglas)
                    });
                } else {
                    return new JsonResult(new {
                        Message = "Oglas ne postoji"
                    });
                }
            }catch(Exception e) {
                return new JsonResult(new {
                    Message = "Greška prilikom pokušaja brisanja oglasa"
                });
            }
        }

        [HttpGet]
        public async Task<JsonResult> oglasi() {

            try {
                var oglasi = await ctx.Oglas.Include(o => o.FkKnjigaNavigation).ThenInclude(k => k.JeZanra).ThenInclude(z => z.IdZanrNavigation)
                                            .Include(o => o.FkUserNavigation).ToListAsync();
                return new JsonResult(new {
                    Message = "Uspješno vraćeni svi oglasi.",
                    Oglasi = oglasi
                });
            }catch(Exception e) {
                return new JsonResult(new {
                    Message = "Pogreška prilikom dohvaćanja svih oglasa"
                });
            }

        }

        [HttpGet]
        public async Task<JsonResult> mojiOglasi() {

            var stringToken = Request.Headers[HeaderNames.Authorization].ToString().Substring(7);
            Korisnik korisnik = AuthController.getUserFromToken(stringToken);

            try {
                var oglasi = await ctx.Oglas.Where(o => o.FkUser == korisnik.IdUser)
                                            .Include(o => o.FkKnjigaNavigation).ThenInclude(k => k.JeZanra).ThenInclude(z => z.IdZanrNavigation).ToListAsync();
                return new JsonResult(new {
                    Message = "Uspješno vraćeni oglasi korisnika",
                    Oglasi = oglasi
                });
            }catch(Exception e) {
                return new JsonResult(new {
                    Message = "Pogreška prilikom vraćanja korisnikovih oglasa"
                });
            }
        }

        [HttpGet]
        public async Task<JsonResult> korisnikOglasi(int id) {

            try {
                var korisnik = await ctx.Korisnik.FindAsync(id);

                var oglasi = await ctx.Oglas.Where(o => o.FkUser == korisnik.IdUser)
                                            .Include(o => o.FkUserNavigation)
                                            .Include(o => o.FkKnjigaNavigation).ThenInclude(k => k.JeZanra).ThenInclude(z => z.IdZanrNavigation).ToListAsync();
                return new JsonResult(new {
                    Message = "Uspješno vraćeni oglasi usera pod id-em " + id,
                    Oglasi = oglasi
                });
            }catch(Exception e) {
                return new JsonResult(new {
                    Message = "Pogreška prilikom dohvaćanja oglasa od usera pod id = " + id
                });
            }
        }

        [HttpGet]
        public async Task<JsonResult> oglas(int id) {

            try {
                var oglas = await ctx.Oglas.Where(o => o.IdOglas == id)
                                           .Include(o => o.FkKnjigaNavigation).ThenInclude(k => k.JeZanra).ThenInclude(z => z.IdZanrNavigation)
                                           .Include(o => o.FkUserNavigation).FirstOrDefaultAsync();
                if(oglas != null) {
                    return new JsonResult(new {
                        Message = "Uspješno vraćen oglas pod id-em " + id,
                        Oglas = oglas
                    });
                } else {
                    return new JsonResult(new {
                        Message = "Oglas pod id-em " + id + " ne postoji"
                    });
                }
               
            }catch(Exception e) {
                return new JsonResult(new {
                    Message = "Pogreška prilikom vraćanja oglasa pod id-em " + id
                });
            }

        }

    }

    
}
