﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZamjenaKnjiga.Model;

namespace ZamjenaKnjiga.Controllers {
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class GradController : ControllerBase {

        private readonly ProjektDatabaseContext ctx;

        public GradController(ProjektDatabaseContext ctx) {
            this.ctx = ctx;
        }

        [HttpGet]
        public async Task<JsonResult> gradovi() {
            try {
                var gradovi = await ctx.Grad.Include(g =>g.FkDrzavaNavigation).ToListAsync();
                return new JsonResult(new {
                    Message = "Uspješno vraćeni gradovi",
                    Gradovi = gradovi
                });
            }catch(Exception e) {
                return new JsonResult(new {
                    Message = "Pogreška prilikom vraćanja gradova"
                });
            }
        }

    }
}
