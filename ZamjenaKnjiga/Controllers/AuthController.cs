﻿using JWT.Algorithms;
using JWT.Builder;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ZamjenaKnjiga.DTO;
using ZamjenaKnjiga.Model;

namespace ZamjenaKnjiga.Controllers {
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class AuthController : ControllerBase {

        private readonly ProjektDatabaseContext ctx;

        private IConfiguration configuration;

        public AuthController(ProjektDatabaseContext ctx, IConfiguration configuration) {
            this.ctx = ctx;
            this.configuration = configuration;
        }

        [HttpPost]
        public async Task<JsonResult> Register(Korisnik req) {

            var korisnikUBazi = await ctx.Korisnik.AsNoTracking().Where(k => k.Email == req.Email).FirstOrDefaultAsync();
            if (korisnikUBazi != null) {
                return new JsonResult(new {
                    Message = "Korisnik s tim emailom vec postoji"
                });
            }

            korisnikUBazi = await ctx.Korisnik.AsNoTracking().Where(k => k.Username == req.Username).FirstOrDefaultAsync();
            if (korisnikUBazi != null) {
                return new JsonResult(new {
                    Message = "Korisnik s tim usernameom vec postoji!"
                });
            }

            req.Uloga = "user";

            await ctx.AddAsync(req);
            await ctx.SaveChangesAsync();

            return new JsonResult(new {
                Message = "Korisnik uspjesno registriran.",
                Korisnik = req
            });
        }

        private string GenerateJWT(Korisnik korisnik) {

            var issuer = configuration["Jwt:Issuer"];
            var audience = configuration["Jwt:Audience"];

            var expiry = DateTime.Now.AddMinutes(120);

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: issuer, 
                audience: audience, 
                expires: expiry, 
                signingCredentials: credentials);

            token.Payload["korisnik"] = korisnik;

            var tokenHandler = new JwtSecurityTokenHandler();
            var stringToken = tokenHandler.WriteToken(token);

            return stringToken;
        }

        [HttpPost]
        public async Task<JsonResult> Login(LoginRequest login) {

            var korisnik = await ctx.Korisnik.AsNoTracking().Where( k => k.Username == login.Username).FirstOrDefaultAsync();
            if(korisnik != null) {
                if(korisnik.Password != login.Password) {
                    return new JsonResult(new {
                        Message = "Netocna lozinka!"
                    });
                }

                var tokenString = GenerateJWT(korisnik);

                return new JsonResult(new {
                    Message = "Korisnik je uspješno ulogiran",
                    Token = tokenString,
                    Korisnik = korisnik
                });
            } else {

                return new JsonResult(new {
                    Message = "Ne postoji korisnik s tim korisnickim imenom"
                });
            }
        }

        public static Korisnik getUserFromToken(string stringToken) {

            var token = new JwtSecurityTokenHandler().ReadToken(stringToken) as JwtSecurityToken;
            JObject korisnik = token.Payload["korisnik"] as JObject;
            Korisnik k = korisnik.ToObject<Korisnik>();

            return k;
        }

        [Authorize]
        [HttpGet]
        public JsonResult CurrentUser() {

            var stringToken = Request.Headers[HeaderNames.Authorization].ToString().Substring(7);
            Console.WriteLine(stringToken);
            var korisnik = getUserFromToken(stringToken);

            return new JsonResult(new {
                Korisnik = korisnik
            });
        }

        [HttpGet]
        public async Task<JsonResult> korisnik(int id) {

            try {
                var korisnik = await ctx.Korisnik.FindAsync(id);

                if(korisnik != null) {
                    return new JsonResult(new {
                        Message = "Uspješno vraćen korisnik pod id-em " + id,
                        Korisnik = korisnik
                    });
                } else {
                    return new JsonResult(new {
                        Message = "Korisnik pod tim id-em " + id + " ne postoji"
                    });
                }
            }catch(Exception e) {
                return new JsonResult(new {
                    Message = "Pogreška prilikom vraćanja korisnika pod id-em " + id
                });
            }

        }

    }

    
}
